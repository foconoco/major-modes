;; create the list for font-lock.
;; each class of keyword is given a particular face

(defface adhoc-deleted
  `((t (:inherit diff-indicator-removed :background "unspecified-bg")))
  "face for deleted connections"
  :group 'adhoc-mode)

(defvar adhoc-deleted-face 'adhoc-deleted)

(defface adhoc-created
  `((t (:inherit diff-indicator-added :background "unspecified-bg")))
  "face for created connections"
  :group 'adhoc-mode)
(defvar adhoc-created-face 'adhoc-created)

(setq adhoc-font-lock-keywords
      `((,(rx bol "C" (1+ space) (group-n 1 (1+ (not (any "/" space))))
              (0+ (not space)) (1+ space)
              (group-n 2 (1+ (not (any "/" space "\n" )))))
         (1 adhoc-created-face) (2 adhoc-created-face))
        (,(rx bol "D" (1+ space) (group-n 1 (1+ (not (any "/" space "\n" )))))
         (1 adhoc-deleted-face))
        (,(rx bol "CP" (1+ space) (group-n 1 (1+ (not (any "/" space "\n" )))))
         (1 font-lock-warning-face) )
        (,(rx bol (group (1+ alpha))) 1 font-lock-keyword-face)
        (,(rx space "-" (group (1+ (any word "_")))) 1 font-lock-function-name-face)
        (,(rx bol "V" (1+ space) "ipgen_connect" (opt "_par") (1+ space) (group-n 1 (1+ (any word "_")))
              (1+ space) (1+ (any word "_:[]")) (1+ space)
              (group-n 2 (1+ (any word "_"))))
         (1 font-lock-function-name-face) (2 font-lock-function-name-face))
        (,(rx space (group "in") symbol-end) 1 font-lock-function-name-face)
        (,(rx (group (1+ (not (any space "\n")))) "/") 1 font-lock-builtin-face)))


;;;###autoload
(add-to-list 'auto-mode-alist `(,(rx (or "tieoff" "adhoc_connection") ".txt" eos) . adhoc-mode))

;; define the mode
;;;###autoload
(define-derived-mode adhoc-mode prog-mode "adhoc"
  "Major mode for editing the adhoc_connection.txt file."
  (modify-syntax-entry ?# "< b" adhoc-mode-syntax-table)
  (modify-syntax-entry ?\n "> b" adhoc-mode-syntax-table)
  (modify-syntax-entry ?/ "." adhoc-mode-syntax-table)
  (setq-local comment-start "#")
  (setq-local comment-end "")
  (setq-local font-lock-defaults '(adhoc-font-lock-keywords)))

(provide 'adhoc-mode)
