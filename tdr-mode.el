(setq tdr-font-lock-keywords
      `((,(rx bol (group (or "START" "END") (1+ nonl))) 1 font-lock-function-name-face)
        (,(rx bol (group (1+ num) (opt ":" (1+ num))) (1+ space) "{" (group (1+ (any word "_"))) "}") (1 font-lock-builtin-face) (2 font-lock-variable-name-face))
        (,(rx (group (1+ num) (or "'h" "'b") (1+ num))) 1 font-lock-builtin-face)
        (,(rx bol (group alpha (1+ (any alnum "_")))) 1 font-lock-keyword-face)))


;;;###autoload
(add-to-list 'auto-mode-alist `(,(rx ".tdr" eos) . tdr-mode))

;; define the mode
;;;###autoload
(define-derived-mode tdr-mode fundamental-mode "TDR"
  "Major mode for editing the TDR files"
  (modify-syntax-entry ?# "< b" tdr-mode-syntax-table)
  (modify-syntax-entry ?\n "> b" tdr-mode-syntax-table)
  (setq-local comment-start "#")
  (setq-local comment-end "")
  (setq-local font-lock-defaults '(tdr-font-lock-keywords)))

(provide 'tdr-mode)
