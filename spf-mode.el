;;; spf-mode.el --- major mode for spf and espf -*- lexical-binding: t; -*-

;; Copyright (C) 2018  Troy Hinckley

;;; Code:

(defalias 'spf-syntax-propertize-function
  (syntax-propertize-rules ("\\('\\)" (1 "."))))

(setq spf-font-lock-keywords
      `((,(rx (or "set" "capture" "compare" "mask") (1+ space) symbol-start (group (1+ (any alnum "_")))) 1 font-lock-constant-face)
        (,(rx "'" (or "FILL:" (any "bdh")) (group (1+ (any hex "_")))) 1 font-lock-builtin-face)
        (,(rx "'" (group (or "DEFAULT" "PREVIOUS"))) 1 font-lock-builtin-face)
        (,(rx symbol-start (group (1+ digit)) symbol-end) 1 font-lock-builtin-face)
        (,(rx symbol-start (group (1+ digit)) (optional (1+ space)) (optional ";") (0+ space) eol) 1 font-lock-builtin-face)
        (,(rx bol (optional (1+ space)) (or "label" "@set")) . font-lock-warning-face)
        (,(rx bol (optional (1+ space)) symbol-start (group (1+ (any alnum "_")))) 1)))

;;;###autoload
(add-to-list 'auto-mode-alist (cons (rx "." (optional "e") "spf" eos)  'spf-mode))

;;;###autoload
(define-derived-mode spf-mode perl-mode "spf"
  (font-lock-add-keywords 'spf-mode spf-font-lock-keywords)
  (add-function :before (local 'syntax-propertize-function) 'spf-syntax-propertize-function))

(provide 'spf-mode)
;;; spf-mode.el ends here
