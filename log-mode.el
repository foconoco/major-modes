;;; log-mode.el --- major mode for log files -*- lexical-binding: t; -*-

;; Copyright (C) 2018  Troy Hinckley

;;; Code:

;;;###autoload
(add-to-list 'auto-mode-alist `(,(rx "." (or "rpt" "log" "run") (optional "." (1+ num)) eos) . log-mode))

;;;###autoload
(define-derived-mode log-mode fundamental-mode "log"
  (modify-syntax-entry ?\" "." log-mode-syntax-table)
  (when (string-match-p (rx (or "espfmodel" "dft_build" "vclp"))
                        (or (buffer-file-name) ""))
    (ansi-color-apply-on-region (point-min) (point-max))
    (when (and (not buffer-read-only)
               (buffer-modified-p))
      (save-buffer)))
  (when (< (buffer-size) 50000000)
    (compilation-minor-mode))
  (setq-local buffer-read-only t))


(provide 'log-mode)
;;; log-mode.el ends here
